
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.FocusEvent;

public class controlador implements ActionListener {

    private ProductoForma vista;
    private Producto producto;
    private ProductoDAO productoDao;
    private DefaultTableModel dtmTabla;

    public controlador(ProductoForma vista, Producto producto, ProductoDAO productoDao) {
        this.vista = vista;
        this.producto = producto;
        this.productoDao = productoDao;
    }

    public void iniciar() {
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);

        asignarcontrol();
    
        dtmTabla = new DefaultTableModel();
        dtmTabla.addColumn("Núm. Prod.");
        dtmTabla.addColumn("Descripción");
    }

    private void asignarcontrol() {
        vista.getCmdAgregar().addActionListener(this);
        vista.getCmdEliminar().addActionListener(this);
        vista.getCmdSalir().addActionListener(this);
        vista.getTxtDescripcion().addActionListener(this);
        vista.getTxtNumProducto().addActionListener(this);
    }

    private void actualizarTabla() {

        List<Producto> ListaProducto = productoDao.listarRegistros();
        Object Fila[] = new Object[3];
        for (Producto P : ListaProducto) {

            Fila[0] = P.getNumProducto();
            Fila[1] = P.getDescripcion();

        }
        dtmTabla.addRow(Fila);
        vista.tblProductos.setModel(dtmTabla);
    }

    private void agregar() {
        try {
            producto.setDescripcion(vista.getTxtDescripcion().getText());
            producto.setNumProducto(vista.getTxtNumProducto().getText());
            productoDao.agregarRegistro(producto);
            actualizarTabla();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }

    private void eliminar() {
        producto.setDescripcion(vista.getTxtDescripcion().getText());
            producto.setNumProducto(vista.getTxtNumProducto().getText());
        productoDao.eliminarRegistro(producto);
        actualizarTabla();
    }

    private void cerrar() {
        System.exit(0);
    }

    private void productolost() {
        producto.setNumProducto(vista.getTxtNumProducto().getText());
    }

    private void descripcionlost() {
        producto.setDescripcion(vista.getTxtDescripcion().getText());
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.getCmdAgregar()) {
            agregar();
        }
        if (e.getSource() == vista.getCmdEliminar()) {
            eliminar();
        }
        if (e.getSource() == vista.getCmdSalir()) {
            cerrar();
        }
    }

    public void focusLost(ActionEvent e) {
        if (e.getSource() == vista.getTxtDescripcion().getFocusListeners()) {
            producto.setDescripcion(vista.getTxtDescripcion().getText());
        }
        if (e.getSource() == vista.getTxtNumProducto().getFocusListeners()) {
            producto.setNumProducto(vista.getTxtNumProducto().getText());
        }
    }

}
